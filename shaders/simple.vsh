#version 330 core

layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 norm;
layout(location = 2) in vec2 uv;
layout(location = 3) in vec3 bary;

out vec3 col_bary;

uniform mat4 MVP;
uniform vec3 objScale;

void main()
{
	gl_Position = MVP*vec4(pos, 1);
	col_bary = bary;
}
