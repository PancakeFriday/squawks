#pragma once

#include <array>
#include <cassert>
#include <GLFW/glfw3.h>

/*
===========================================
Input Object

Handles keyboard, mouse input, Touch, controller as well?
===========================================
*/
class Input
{
	private:
		Input()
		{
			for(auto& v:mKeyboardHandler.key)
			{
				v = false;
			}
			for(auto& v:mModsHandler.mod)
			{
				v = false;
			}
			for(auto& v:mMouseHandler.key)
			{
				v = false;
			}
			mScrollHandler.x = mScrollHandler.y = 0;
		}

		Input(Input const&)               = delete;
        void operator=(Input const&)  = delete;

		static struct Keyboard
		{
			bool key[GLFW_KEY_LAST];
		} mKeyboardHandler;

		static struct Mods
		{
			bool mod[GLFW_MOD_SUPER]; // this is the last one
		} mModsHandler;

		static struct Mouse
		{
			bool key[GLFW_MOUSE_BUTTON_LAST];
		} mMouseHandler;

		static struct Scroll
		{
			double x;
			double y;
		} mScrollHandler;

		static void keyCallback(GLFWwindow* /*window*/, int key, int /*scancode*/, int action, int mods)
		{
			if(action == GLFW_PRESS)
			{
				mKeyboardHandler.key[key] = true;
				mModsHandler.mod[mods] = true;
			}
			if(action == GLFW_RELEASE)
			{
				mKeyboardHandler.key[key] = false;
				mModsHandler.mod[mods] = false;
			}
		}

		static void mouseCallback(GLFWwindow* /*window*/, int button, int action, int mods)
		{
			if(action == GLFW_PRESS)
			{
				mMouseHandler.key[button] = true;
				mModsHandler.mod[mods] = true;
			}
			if(action == GLFW_RELEASE)
			{
				mMouseHandler.key[button] = false;
				mModsHandler.mod[mods] = false;
			}
		}

		static void scrollCallback(GLFWwindow* /*window*/, double xoffset, double yoffset)
		{
			mScrollHandler.x = xoffset;
			mScrollHandler.y = yoffset;
		}

	public:
		static Input& getInstance()
		{
			static Input instance;

			return instance;
		}

		static void setCallbacks(GLFWwindow* window)
		{
			glfwSetKeyCallback(window, keyCallback);
			glfwSetMouseButtonCallback(window, mouseCallback);
			glfwSetScrollCallback(window, scrollCallback);
		}

		static bool getKey(int key) // true if pressed
		{
			assert(key <= GLFW_KEY_LAST);
			return mKeyboardHandler.key[key];
		}

		static bool getMods(int mods)
		{
			assert(mods <= GLFW_MOD_SUPER);
			return mModsHandler.mod[mods];
		}

		static bool getMouseButton(int button)
		{
			assert(button <= GLFW_MOUSE_BUTTON_LAST);
			return mMouseHandler.key[button];
		}

		static void getScroll(double& x, double& y)
		{
			x = mScrollHandler.x;
			y = mScrollHandler.y;
		}
};
