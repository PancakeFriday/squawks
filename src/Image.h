#include <string>

class Image
{
	private:
		std::string mFile;
		unsigned char* mData;
		int mWidth;
		int mHeight;
		int mBpp;

	public:
		Image(std::string file);
		~Image();

		unsigned char* loadFile(std::string file = "");
		std::string getFile();
		unsigned char* getData();
		int getWidth();
		int getHeight();
		int getBpp();

		void setFile(std::string file);
};
