#version 330 core
in vec2 uv_frag;
in vec2 norm_frag;

out vec4 color;

void main()
{
	color = vec4(uv_frag.x,uv_frag.y,0,1);
}
