/*
 Camera.h
 OpenGL Camera Code
 Capable of 2 modes, orthogonal, and free
 Quaternion camera code adapted from: http://hamelot.co.uk/visualization/opengl-camera/
 initially written by Hammad Mazhar (http://hamelot.co.uk/visualization/moderngl-camera/)
 modified by Robin Eberhard
 */
#ifndef CAMERA_H
#define CAMERA_H

#include <math.h>

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Logger.h"

enum CameraType {
	ORTHO, FREE
};
enum CameraDirection {
	UP=1, DOWN=2, LEFT=4, RIGHT=8, FORWARD=16, BACKWARD=32
};

class Camera {
	public:
		Camera();
		~Camera();

		void reset();
		void fullReset();
		//This function updates the camera
		//Depending on the current camera mode, the projection and viewport matricies are computed
		//Then the position and location of the camera is updated
		void update(float dt);

		//Given a specific moving direction, the camera will be moved in the appropriate direction
		//For a spherical camera this will be around the look_at point
		//For a free camera a delta will be computed for the direction of movement.
		void move(CameraDirection dir);
		void rotate2D(float x, float y);
		//Change the pitch (up, down) for the free camera
		void addToPitch(float degrees);
		//Change heading (left, right) for the free camera
		void addToHeading(float degrees);

		//This should be a function of the editor if anything
		//Change the heading and pitch of the camera based on the 2d movement of the mouse
		//void Move2D(int x, int y);

		//Setting Functions
		//Changes the camera mode, only three valid modes, Ortho, Free, and Spherical
		void setMode(CameraType cam_mode);
		//Set the position of the camera
		void setPosition(glm::vec3 pos);
		//Set's the look at point for the camera
		void setLookAt(glm::vec3 point);
		//Changes the Field of View (FOV) for the camera
		void setFOV(float fov);
		void setAspectRatio(float aspect1);
		//Change the clipping distance for the camera
		void setClipping(double near_clip_distance, double far_clip_distance);

		//Getting Functions
		CameraType getMode();
		void getMatricies(glm::mat4 &P, glm::mat4 &V);


		CameraType mCameraMode;

		float mAspect;
		float mFieldOfView;
		float mNearClip;
		float mFarClip;

		float mHeading;
		float mPitch;
		float mSpeed;

		float mMaxPitch;
		float mMaxHeading;

		glm::vec3 m_vec3_pos;
		glm::vec3 m_vec3_posDelta;
		glm::vec3 m_vec3_lookAt;
		glm::vec3 m_vec3_dir;
		glm::vec3 m_vec3_up;

		uint32_t mMouseState;
		glm::vec3 m_vec3_mousePos;
		glm::vec3 m_vec3_mouseDelta;

		glm::mat4 m_mat4_projection;
		glm::mat4 m_mat4_view;
		glm::mat4 m_mat4_VP;

};
#endif
