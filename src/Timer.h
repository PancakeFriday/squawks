#include <map>
#include <string>

#include <GLFW/glfw3.h>

#include "Logger.h"

class Timer
{
	private:
		bool findIdentifier(std::string identifier);

		// count those down
		std::map<std::string, float> mTimesNeg;
		// those up
		std::map<std::string, float> mTimesPos;

	public:
		Timer();
		void update(float dt);

		void erase(std::string identifier);

		float delayFunc(std::string identifier, float time,
				std::function<void ()> funcDelayNotEnd, std::function<void ()> funcDelayEnd);
		float getTime(std::string identifier,
				std::function<float (float)> funcModifier = [](float f){return f;});
};
