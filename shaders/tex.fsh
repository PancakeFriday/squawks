#version 330 core
in vec2 uv_frag;
in vec2 norm_frag;

uniform sampler2D TextureSampler;

out vec4 color;

void main()
{
	color = texture( TextureSampler, uv_frag );
}
