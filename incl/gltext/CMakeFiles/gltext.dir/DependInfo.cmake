# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/robin/Workspace/c++/squawks/incl/gltext/gltext.cpp" "/home/robin/Workspace/c++/squawks/incl/gltext/CMakeFiles/gltext.dir/gltext.cpp.o"
  "/home/robin/Workspace/c++/squawks/incl/gltext/harfbuzz/hb-blob.cc" "/home/robin/Workspace/c++/squawks/incl/gltext/CMakeFiles/gltext.dir/harfbuzz/hb-blob.cc.o"
  "/home/robin/Workspace/c++/squawks/incl/gltext/harfbuzz/hb-buffer.cc" "/home/robin/Workspace/c++/squawks/incl/gltext/CMakeFiles/gltext.dir/harfbuzz/hb-buffer.cc.o"
  "/home/robin/Workspace/c++/squawks/incl/gltext/harfbuzz/hb-common.cc" "/home/robin/Workspace/c++/squawks/incl/gltext/CMakeFiles/gltext.dir/harfbuzz/hb-common.cc.o"
  "/home/robin/Workspace/c++/squawks/incl/gltext/harfbuzz/hb-fallback-shape.cc" "/home/robin/Workspace/c++/squawks/incl/gltext/CMakeFiles/gltext.dir/harfbuzz/hb-fallback-shape.cc.o"
  "/home/robin/Workspace/c++/squawks/incl/gltext/harfbuzz/hb-font.cc" "/home/robin/Workspace/c++/squawks/incl/gltext/CMakeFiles/gltext.dir/harfbuzz/hb-font.cc.o"
  "/home/robin/Workspace/c++/squawks/incl/gltext/harfbuzz/hb-ft.cc" "/home/robin/Workspace/c++/squawks/incl/gltext/CMakeFiles/gltext.dir/harfbuzz/hb-ft.cc.o"
  "/home/robin/Workspace/c++/squawks/incl/gltext/harfbuzz/hb-ot-layout.cc" "/home/robin/Workspace/c++/squawks/incl/gltext/CMakeFiles/gltext.dir/harfbuzz/hb-ot-layout.cc.o"
  "/home/robin/Workspace/c++/squawks/incl/gltext/harfbuzz/hb-ot-map.cc" "/home/robin/Workspace/c++/squawks/incl/gltext/CMakeFiles/gltext.dir/harfbuzz/hb-ot-map.cc.o"
  "/home/robin/Workspace/c++/squawks/incl/gltext/harfbuzz/hb-ot-shape-complex-arabic.cc" "/home/robin/Workspace/c++/squawks/incl/gltext/CMakeFiles/gltext.dir/harfbuzz/hb-ot-shape-complex-arabic.cc.o"
  "/home/robin/Workspace/c++/squawks/incl/gltext/harfbuzz/hb-ot-shape-complex-indic.cc" "/home/robin/Workspace/c++/squawks/incl/gltext/CMakeFiles/gltext.dir/harfbuzz/hb-ot-shape-complex-indic.cc.o"
  "/home/robin/Workspace/c++/squawks/incl/gltext/harfbuzz/hb-ot-shape-complex-misc.cc" "/home/robin/Workspace/c++/squawks/incl/gltext/CMakeFiles/gltext.dir/harfbuzz/hb-ot-shape-complex-misc.cc.o"
  "/home/robin/Workspace/c++/squawks/incl/gltext/harfbuzz/hb-ot-shape-normalize.cc" "/home/robin/Workspace/c++/squawks/incl/gltext/CMakeFiles/gltext.dir/harfbuzz/hb-ot-shape-normalize.cc.o"
  "/home/robin/Workspace/c++/squawks/incl/gltext/harfbuzz/hb-ot-shape.cc" "/home/robin/Workspace/c++/squawks/incl/gltext/CMakeFiles/gltext.dir/harfbuzz/hb-ot-shape.cc.o"
  "/home/robin/Workspace/c++/squawks/incl/gltext/harfbuzz/hb-ot-tag.cc" "/home/robin/Workspace/c++/squawks/incl/gltext/CMakeFiles/gltext.dir/harfbuzz/hb-ot-tag.cc.o"
  "/home/robin/Workspace/c++/squawks/incl/gltext/harfbuzz/hb-shape.cc" "/home/robin/Workspace/c++/squawks/incl/gltext/CMakeFiles/gltext.dir/harfbuzz/hb-shape.cc.o"
  "/home/robin/Workspace/c++/squawks/incl/gltext/harfbuzz/hb-unicode.cc" "/home/robin/Workspace/c++/squawks/incl/gltext/CMakeFiles/gltext.dir/harfbuzz/hb-unicode.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "HAVE_OT=1"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/freetype2"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
