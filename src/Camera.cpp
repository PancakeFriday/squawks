#include "Camera.h"
using namespace std;

Camera::Camera() {
	mCameraMode = FREE;

	mAspect = 16.0f/9.0f;
	mFieldOfView = 45.0f;
	mNearClip = 0.1f;
	mFarClip = 1000.0f;

	mHeading = 0.0f;
	mPitch = 0.0f;
	mSpeed = 2.0f;

	mMaxHeading = 5.0f;
	mMaxPitch = 5.0f;

	mMouseState = 0;

	m_vec3_pos = glm::vec3(0,0.3,5.8);
	m_vec3_posDelta = glm::vec3(0, 0, 0);
	m_vec3_lookAt = m_vec3_pos + glm::vec3(0,0,-1);
	m_vec3_up = glm::vec3(0, 1, 0);

}
Camera::~Camera() {
}

void Camera::reset() {
	mHeading = mPitch = 0.0f;
	m_vec3_posDelta = glm::vec3(0.0f);
	m_vec3_up = glm::vec3(0.0f, 1.0f, 0.0f);
	m_vec3_lookAt = m_vec3_pos + glm::vec3(0.0f, 0.0f, 1.0f);
	m_vec3_pos.y = 1.0f;
}

void Camera::fullReset() {
	reset();
	m_vec3_pos = glm::vec3(0,1,-9);
}

void Camera::update(float dt) {
	m_vec3_dir = glm::normalize(m_vec3_lookAt - m_vec3_pos);

	// if the mouse function hasn't been called, mMouseState will be 0
	mMouseState = fmax(0,mMouseState - 1);

	if (mCameraMode == ORTHO) {
		//our projection matrix will be an orthogonal one in this case
		//if the values are not floating point, this command does not work properly
		//need to multiply by aspect!!! (otherise will not scale properly)
		m_mat4_projection = glm::ortho(-1.5f * float(mAspect), 1.5f * float(mAspect), -1.5f, 1.5f, -10.0f, 10.f);
	} else if (mCameraMode == FREE) {
		m_mat4_projection = glm::perspective(mFieldOfView, mAspect, mNearClip, mFarClip);
		//detmine axis for pitch rotation
		glm::vec3 axis = glm::cross(m_vec3_dir, m_vec3_up);
		//compute quaternion for pitch based on the camera pitch angle
		glm::quat pitch_quat = glm::angleAxis(mPitch, axis);
		//determine heading quaternion from the camera up vector and the heading angle
		glm::quat heading_quat = glm::angleAxis(mHeading, m_vec3_up);
		//add the two quaternions
		glm::quat temp = glm::cross(pitch_quat, heading_quat);
		temp = glm::normalize(temp);
		//update the direction from the quaternion
		m_vec3_dir = glm::rotate(temp, m_vec3_dir);
		//add the camera delta
		m_vec3_pos += m_vec3_posDelta * mSpeed * dt;
		//set the look at to be infront of the camera
		m_vec3_lookAt = m_vec3_pos + m_vec3_dir * 1.0f;
		//damping for smooth camera
		mHeading *= .5f;
		mPitch *= .5f;
		m_vec3_posDelta = m_vec3_posDelta * .8f;
	}
	//compute the MVP
	m_mat4_view = glm::lookAt(m_vec3_pos, m_vec3_lookAt, m_vec3_up);
	m_mat4_VP = m_mat4_projection * m_mat4_view;
}

//Setting Functions
void Camera::setMode(CameraType cam_mode) {
	mCameraMode = cam_mode;
	m_vec3_up = glm::vec3(0, 1, 0);
}

void Camera::setPosition(glm::vec3 pos) {
	m_vec3_pos = pos;
}

void Camera::setLookAt(glm::vec3 point) {
	m_vec3_lookAt = point;
}
void Camera::setFOV(float fov) {
	mFieldOfView = fov;
}
void Camera::setAspectRatio(float aspect1)
{
	mAspect = aspect1;
}

void Camera::setClipping(double near_clip_distance, double far_clip_distance) {
	mNearClip = near_clip_distance;
	mFarClip = far_clip_distance;
}

void Camera::move(CameraDirection dir) {
	if (mCameraMode == FREE) {
		switch (dir) {
			case UP:
				m_vec3_posDelta += m_vec3_up * mSpeed;
				break;
			case DOWN:
				m_vec3_posDelta -= m_vec3_up * mSpeed;
				break;
			case LEFT:
				m_vec3_posDelta -= glm::cross(m_vec3_dir, m_vec3_up) * mSpeed;
				break;
			case RIGHT:
				m_vec3_posDelta += glm::cross(m_vec3_dir, m_vec3_up) * mSpeed;
				break;
			case FORWARD:
				m_vec3_posDelta += m_vec3_dir * mSpeed * 2.0f;
				break;
			case BACKWARD:
				m_vec3_posDelta -= m_vec3_dir * mSpeed * 2.0f;
				break;
		}
	}
}
void Camera::rotate2D(float x, float y) {
	//compute the mouse delta from the previous mouse position
	if(mMouseState <= 0)
	{
		m_vec3_mousePos = glm::vec3(x,y,0.0f);
	}
	m_vec3_mouseDelta = m_vec3_mousePos - glm::vec3(x, y, 0.0f);
	//if the camera is moving, meaning that the mouse was clicked and dragged, change the pitch and heading
	addToHeading(.08f * m_vec3_mouseDelta.x);
	addToPitch(.08f * m_vec3_mouseDelta.y);
	m_vec3_mousePos = glm::vec3(x, y, 0.0f);
	mMouseState = 2;
}
void Camera::addToPitch(float degrees) {
	//Check bounds with the max pitch rate so that we aren't moving too fast
	if (degrees < -mMaxPitch) {
		degrees = -mMaxPitch;
	} else if (degrees > mMaxPitch) {
		degrees = mMaxPitch;
	}
	mPitch += degrees;

	//Check bounds for the camera pitch
	if (mPitch > 360.0f) {
		mPitch -= 360.0f;
	} else if (mPitch < -360.0f) {
		mPitch += 360.0f;
	}
}
void Camera::addToHeading(float degrees) {
	//Check bounds with the max heading rate so that we aren't moving too fast
	if (degrees < -mMaxHeading) {
		degrees = -mMaxHeading;
	} else if (degrees > mMaxHeading) {
		degrees = mMaxHeading;
	}
	//This controls how the heading is changed if the camera is pointed straight up or down
	//The heading delta direction changes
	if ((mPitch > 90 && mPitch < 270) || (mPitch < -90 && mPitch > -270)) {
		mHeading -= degrees;
	} else {
		mHeading += degrees;
	}
	//Check bounds for the camera heading
	if (mHeading > 360.0f) {
		mHeading -= 360.0f;
	} else if (mHeading < -360.0f) {
		mHeading += 360.0f;
	}
}

CameraType Camera::getMode() {
	return mCameraMode;
}

void Camera::getMatricies(glm::mat4 &P, glm::mat4 &V) {
	P = m_mat4_projection;
	V = m_mat4_view;
}
