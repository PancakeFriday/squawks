#include "Shader.h"

Shader_prog::Shader_prog()
{

}

Shader_prog::~Shader_prog()
{
	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);
	glDeleteProgram(prog);
}

template <int N>
GLuint Shader_prog::compile(GLuint type, char const *(&source)[N])
{
	GLuint shader = glCreateShader(type);
	glShaderSource(shader, N, source, NULL);
	glCompileShader(shader);

	GLint compiled;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);

	if (!compiled) {
		GLint length;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
		std::string log(length, ' ');
		glGetShaderInfoLog(shader, length, &length, &log[0]);
		throw std::logic_error(log);
		Log(Error) << "Couldn't compile shader: " << log;
		return false;
	}
	Log() << "Successfully compiled shader";
	return shader;
}

template <int N, int M>
void Shader_prog::runChar(GLchar const *(&v_source)[N], GLchar const *(&f_source)[M])
{
	vertex_shader = compile(GL_VERTEX_SHADER, v_source);
	fragment_shader = compile(GL_FRAGMENT_SHADER, f_source);
	prog = glCreateProgram();
	glAttachShader(prog, vertex_shader);
	glAttachShader(prog, fragment_shader);
	glLinkProgram(prog);
}

void Shader_prog::runFile(std::string vertexFile, std::string fragmentFile)
{
	std::string line, vtext, ftext;
	std::ifstream vFile(vertexFile);
	if(!vFile.is_open())
	{
		Log(Error) << "Invalid shader file: " << vertexFile;
	}

	while(std::getline(vFile, line))
	{
		vtext += line + "\n";
	}
	const GLchar* vertexData[1] = { vtext.c_str() };

	std::ifstream fFile(fragmentFile);
	if(!fFile.is_open())
	{
		Log(Error) << "Invalid shader file: " << fragmentFile;
	}
	Log() << "Compiling " + vertexFile + " and " + fragmentFile + "...";

	while(std::getline(fFile, line))
	{
		ftext += line + "\n";
	}
	const GLchar* fragmentData[1] = {  ftext.c_str() };
	runChar(vertexData, fragmentData);

	vFile.close();
	fFile.close();
}

GLuint Shader_prog::getID()
{
	return prog;
}

void Shader_prog::use()
{
	glUseProgram(prog);
}
