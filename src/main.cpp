#include "Squawks.h"

int main( void )
{
	Squawks squawks;

	if( squawks.init() == -1 )
	{
		return -1;
	}

	squawks.mainLoop();

	if( squawks.close() == -1 )
	{
		return -1;
	}

	return 0;
}
