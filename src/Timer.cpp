#include "Timer.h"

Timer::Timer()
{

}

void Timer::update(float dt)
{
	for(auto it=mTimesNeg.begin(); it!=mTimesNeg.end(); ++it)
	{
		it->second -= dt;
	}
	for(auto it=mTimesPos.begin(); it!=mTimesPos.end(); ++it)
	{
		it->second += dt;
	}
}

void Timer::erase(std::string identifier)
{
	mTimesNeg.erase(identifier);
	mTimesPos.erase(identifier);
}

bool Timer::findIdentifier(std::string identifier)
{
	std::map<std::string, float>::const_iterator it = mTimesNeg.find(identifier);
	std::map<std::string, float>::const_iterator jt = mTimesPos.find(identifier);
	return (it!=mTimesNeg.end()) || (jt!=mTimesPos.end());
}

float Timer::delayFunc(std::string identifier, float time,
		std::function<void ()> funcDelayNotEnd, std::function<void ()> funcDelayEnd)
{
	if(!findIdentifier(identifier))
	{
		mTimesNeg[identifier] = time;
		funcDelayNotEnd();
	}
	else
	{
		if(mTimesNeg[identifier] <= 0)
		{
			funcDelayEnd();
			return 0;
		}
		else
		{
			funcDelayNotEnd();
		}
	}

	return mTimesNeg[identifier];
}

float Timer::getTime(std::string identifier, std::function<float (float)> funcModifier)
{
	if(!findIdentifier(identifier))
	{
		mTimesPos[identifier] = 0;
	}
	return funcModifier(mTimesPos[identifier]);
}
