CPP_FILES := $(wildcard src/*.cpp)
OBJ_FILES := $(addprefix obj/,$(notdir $(CPP_FILES:.cpp=.o)))
LD_PATHS := -Llib/
LD_FLAGS := -lGL -lGLU -lglfw -lGLEW -lgltext -lfreetype
CC_FLAGS := -O0 -Wall -g --std=c++14 -Iincl/

editor: $(OBJ_FILES)
	clang++ $(LD_PATHS) -o $@ $^ $(LD_FLAGS)
obj/%.o: src/%.cpp
	clang++ $(CC_FLAGS) -c -o $@ $<
