#include <bitset>

#include "defines.h"
#include "Camera.h"
#include "GUI.h"
#include "Logger.h"
#include "Object.h"
#include "Timer.h"

class Editor
{
	private:
		static GLFWwindow* mWindow;

		ptr_u<Camera> mCamera;

		ptr_s<Timer> mTimer;

		glm::mat4 m_mat4_projection;
		glm::mat4 m_mat4_view;

		ptr_u<Object::Plane> mPlaneXZ;
		ptr_u<Object::Plane> mPlaneXY;

		ptr_u<GUI::Node> mNode;
		ptr_s<GUI::BackgroundImage> mBox1;
		ptr_s<GUI::BackgroundImage> mBox2;

		int mCameraDir;

		void moveCamera();

		static GLFWwindow* getWindow();

		static void moveCameraCB(GLFWwindow* window, int key, int scancode, int action, int mods);
		// needed for glfw callbacks
		static void scrollCB(GLFWwindow* window, double x, double y);
	public:
		Editor(GLFWwindow* window);
		~Editor();
		void input();
		void update(float dt);
		void render();
};
