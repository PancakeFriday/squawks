#pragma once

#include <fstream>
#include <stdexcept>
#include <string>

#include <GL/glew.h>

#include "Logger.h"

class Shader_prog {
	private:
		GLuint vertex_shader, fragment_shader, prog;

		template <int N>
		GLuint compile(GLuint type, char const *(&source)[N]);

	public:
		Shader_prog();
		template <int N, int M>
		void runChar(GLchar const *(&v_source)[N], GLchar const *(&f_source)[M]);
		void runFile(std::string vertexFile, std::string fragmentFile);
		GLuint getID();
		void use();

		~Shader_prog();
};
