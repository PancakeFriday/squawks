//------------------------------
//-- Copyright Robin Eberhard --
//-- 2015 ----------------------
//------------------------------

#pragma once

// Include the logger
#include "Logger.h"

// Include standard headers
#include <math.h>
#include <memory>
#include <stdio.h>
#include <stdlib.h>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

// Diz ma stuff
#include "defines.h"
#include "Editor.h"
#include "Input.h"
#include "Object.h"
#include "Shader.h"
#include "Vertex.h"

class Squawks
{
	private:
		GLFWwindow* mWindow;
		std::unique_ptr<Editor> editor;

		GLuint mVertexArrayID;

		bool mIsRunning;
		float mDeltaTime;
		float mPrevFrameTime;
		float mCurrentFrameTime;

		void input();
		void update();
		void render();

	public:
		Squawks();
		~Squawks();
		int init();
		int mainLoop();
		int close();
};
