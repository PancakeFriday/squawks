#include "Editor.h"
#include "gltext/gltext.hpp"

Editor::Editor(GLFWwindow* window)
{
	mWindow = window;
	mCamera = std::make_unique<Camera>();
	mTimer = std::make_unique<Timer>();

	mPlaneXZ = std::make_unique<Object::Plane>();
	mPlaneXY = std::make_unique<Object::Plane>();
	mPlaneXY->rotateTo(90, glm::vec3(1.0f,0.0f,0.0f));
	mPlaneXZ->scaleTo(glm::vec3(3.0f, 1.0f, 3.0f));
	mPlaneXY->scaleTo(glm::vec3(3.0f, 1.0f, 3.0f));
	mPlaneXY->useShaderFile("shaders/simple.vsh", "shaders/grid.fsh");

	mCameraDir = 0;

	mNode = std::make_unique<GUI::Node>();
	mBox1 = std::make_shared<GUI::BackgroundImage>("res/mario.png",0,0,356,256);
	mBox2 = std::make_shared<GUI::BackgroundImage>("res/mario.png",700,200,100,100);
	mBox1->setImageType(GUI::IMAGE_PROPFILL);
	mBox2->setImageType(GUI::IMAGE_PROPFILL);
	mNode->add(mBox1);
	mNode->add(mBox2);
}

Editor::~Editor()
{

}

void Editor::input()
{
	//glfwSetWindowUserPointer(mWindow, this);
	//glfwSetKeyCallback(mWindow, moveCameraCB);
	//glfwSetScrollCallback(mWindow, scrollCB);
}

void Editor::update(float dt)
{
	moveCamera();

	mTimer->update(dt);
	mCamera->update(dt);
}

void Editor::render()
{
	mCamera->getMatricies(m_mat4_projection, m_mat4_view);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_NOTEQUAL);
	mPlaneXY->render(m_mat4_projection * m_mat4_view);
	mPlaneXZ->render(m_mat4_projection * m_mat4_view);

	mNode->render();
}

void Editor::moveCamera()
{
	if((mCameraDir & UP) == UP)
		mCamera->move(UP);
	if((mCameraDir & DOWN) == DOWN)
		mCamera->move(DOWN);
	if((mCameraDir & LEFT) == LEFT)
		mCamera->move(LEFT);
	if((mCameraDir & RIGHT) == RIGHT)
		mCamera->move(RIGHT);
}

void Editor::moveCameraCB(GLFWwindow* window, int key, int scancode, int action, int)
{
	if(action == GLFW_REPEAT)
		return;

	Editor* editor = ((Editor*)glfwGetWindowUserPointer(window));

	std::bitset<32> set = std::bitset<32>(~0); // all bits 1

	if(key == GLFW_KEY_W)
	{
		set[0] = (action != GLFW_RELEASE);
		editor->mCameraDir = (editor->mCameraDir | UP)&set.to_ulong(); // if set is false, then nvm
	}
	if(key == GLFW_KEY_S)
	{
		set[1] = (action != GLFW_RELEASE);
		editor->mCameraDir = (editor->mCameraDir | DOWN)&set.to_ulong();
	}
	if(key == GLFW_KEY_A)
	{
		set[2] = (action != GLFW_RELEASE);
		editor->mCameraDir = (editor->mCameraDir | LEFT)&set.to_ulong();
	}
	if(key == GLFW_KEY_D)
	{
		set[3] = (action != GLFW_RELEASE);
		editor->mCameraDir = (editor->mCameraDir | RIGHT)&set.to_ulong();
	}

	if(scancode == GLFW_KEY_0)
	{
		// This function checks if the 0 key is pressed twice
		editor->mTimer->delayFunc("reset_camera", 0.1, [&] {
			// action to do if pressed once
			editor->mCamera->reset();
		}, [&] {
			editor->mTimer->delayFunc("reset_camera_full", 0.5, [&] {
				// action to do, if pressed for the second time
				editor->mCamera->fullReset();
				editor->mTimer->erase("reset_camera");
				editor->mTimer->erase("reset_camera_full");
			}, [&] {
				// Time is up, double press is over
				editor->mTimer->erase("reset_camera");
				editor->mTimer->erase("reset_camera_full");
			});
			editor->mTimer->erase("reset_camera");
		});
	}
}

GLFWwindow* Editor::mWindow;
GLFWwindow* Editor::getWindow()
{
	return mWindow;
}

void Editor::scrollCB(GLFWwindow* window, double, double y)
{
	if(y<0)
	{
		((Editor*)glfwGetWindowUserPointer(window))->mCamera->move(BACKWARD);
	}
	else if(y>0)
	{
		((Editor*)glfwGetWindowUserPointer(window))->mCamera->move(FORWARD);
	}
}
