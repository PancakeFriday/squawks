#include "Squawks.h"
logger Log(LOGFILE);
GLFWwindow* Vertex2D::mWindow;

Squawks::Squawks() : mIsRunning(true)
{
}

Squawks::~Squawks()
{

}

int Squawks::init()
{
	Log(Level::Info) << "Starting Engine";

	// Initialise GLFW
	if( !glfwInit() )
	{
		Log(Level::Error) << "Failed to initialize GLFW";
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, 0);

	// Open a window and create its OpenGL context
	float windowWidth = 1600;
	float windowHeight = 900;
	mWindow = glfwCreateWindow( windowWidth, windowHeight, "Cool fucking engine", NULL, NULL);
	if( mWindow == NULL ){
		Log(Level::Error) << "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible.";
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(mWindow);

	// center window
	const GLFWvidmode* vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
	glfwSetWindowPos(mWindow, vidmode->width/2-windowWidth/2, vidmode->height/2 - windowHeight/2);

	// Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		Log(Level::Error) << "Failed to initialize GLEW";
		return -1;
	}

	// Let's start the program
	mDeltaTime = 0;
	mPrevFrameTime = glfwGetTime();
	mCurrentFrameTime = 0;

	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(mWindow, GLFW_STICKY_KEYS, GL_TRUE);

	// Set up coordinate system
	Vertex2D::setWindow(mWindow);

	// Hook up the editor
	editor = std::make_unique<Editor>(mWindow);
	glClearColor(0.0f, 0.2f, 0.4f, 0.0f);

	glGenVertexArrays(1, &mVertexArrayID);
	glBindVertexArray(mVertexArrayID);

	// start the Input singleton
	Input::setCallbacks(mWindow);

	return 0;
}

int Squawks::close()
{
	glfwDestroyWindow(mWindow);
	glfwTerminate();

	return 0;
}

int Squawks::mainLoop()
{
	Log(Level::Info) << "Starting main loop";

	do{
		input();
		update();
		render();
	} // Check if the ESC key was pressed or the window was closed
	while( mIsRunning );

	return 0;
}

void Squawks::input()
{
	if(glfwGetKey(mWindow, GLFW_KEY_ESCAPE ) == GLFW_PRESS || glfwWindowShouldClose(mWindow) != 0)
	{
		mIsRunning = false;
	}

	editor->input();
}

void Squawks::update()
{
	// Get the delta time
	mCurrentFrameTime = glfwGetTime();
	mDeltaTime = mCurrentFrameTime - mPrevFrameTime;
	mPrevFrameTime = glfwGetTime();

	editor->update(mDeltaTime);
}

void Squawks::render()
{
	// Dark blue background
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Enable blending
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// Depth test
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	// render here
	editor->render();

	// Swap buffers
	glfwSwapBuffers(mWindow);
	glfwPollEvents();
}
