#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "Logger.h"

typedef struct Vertex
{
	Vertex(float x,float y,float z)
		: x(x), y(y), z(z) {}
	Vertex(float x,float y,float z, float nx, float ny, float nz)
		: x(x), y(y), z(z), nx(nx), ny(ny), nz(nz) {}
	Vertex(float x,float y,float z, float nx, float ny, float nz, float u, float v)
		: x(x), y(y), z(z), nx(nx), ny(ny), nz(nz), u(u), v(v) {}
	float x, y, z;
	float nx, ny, nz;
	float u,v;
}
Vertex;

typedef struct Vertex2D
{
	public:
		Vertex2D(float tx,float ty)
			: x(tx), y(ty)
		{
			transform();
		}
		Vertex2D(float x,float y, float u, float v)
			: x(x), y(y), u(u), v(v)
		{
			transform();
		}
		Vertex2D(float x,float y, float u, float v, float nx, float ny)
			: x(x), y(y), u(u), v(v), nx(nx), ny(ny)
		{
			transform();
		}
		float x, y;
		float u,v;
		float nx, ny;

		static void setWindow(GLFWwindow* window)
		{
			mWindow = window;
		}

	private:
		static GLFWwindow* mWindow;

		void transform()
		{
			int w,h;
			glfwGetWindowSize(mWindow, &w, &h);
			x = 2*x/w-1;
			y = -(2*y/h)+1;
		}
}
Vertex2D;
