#version 330 core

in vec3 col_bary;
out vec4 color;

uniform vec3 objScale;

void main()
{
	float thickness = 0.01;
	if( any(lessThan(col_bary*objScale, vec3(thickness)))
		&& (greaterThan(col_bary.y*objScale.y, thickness)
			|| lessThan(col_bary.x*objScale.x, thickness)
			|| lessThan(col_bary.z*objScale.z, thickness)
			)
	)
	{
		color = vec4(0.4, 0.7, 0.8, 0.7);
	}
	else{
		color = vec4(0.4, 0.7, 1.0, 0.3);
	}
}
