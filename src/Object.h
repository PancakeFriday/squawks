#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/vector_angle.hpp>

#include <string>
#include <vector>

#include "Logger.h"
#include "Shader.h"
#include "Vertex.h"

namespace Object
{
	enum Mode { MODE_GLOBAL_ORIGIN, MODE_LOCAL_ORIGIN };

	class BaseObject
	{
		protected:
			std::string mObjectType;

			GLuint mVertexbuffer;
			GLuint mBarymetricBuffer;

			std::unique_ptr<Shader_prog> mProgram;

			glm::mat4 m_mat4_model;
			glm::mat4 m_mat4_translate;
			glm::mat4 m_mat4_rotate;
			glm::mat4 m_mat4_scale;

			std::vector<Vertex> mVertices;

		public:
			BaseObject();
			virtual ~BaseObject();

			template <int N, int M>
			void useShaderChar(GLchar const *(&v_source)[N], GLchar const *(&f_source)[M]);
			void useShaderFile(std::string vertexFile, std::string fragmentFile);

			glm::mat4 getModelMatrix();
			virtual void render(glm::mat4 mat_PV);

			virtual BaseObject* translateTo(glm::vec3 pos, Mode mode=MODE_LOCAL_ORIGIN);
			virtual BaseObject* translateBy(glm::vec3 pos, Mode mode=MODE_LOCAL_ORIGIN);
			virtual BaseObject* rotateBy(float angle, glm::vec3 axis, Mode mode=MODE_LOCAL_ORIGIN);
			virtual BaseObject* rotateTo(float angle, glm::vec3 axis, Mode mode=MODE_LOCAL_ORIGIN);
			virtual BaseObject* scaleBy(glm::vec3 scale, Mode mode=MODE_LOCAL_ORIGIN);
			virtual BaseObject* scaleTo(glm::vec3 scale, Mode mode=MODE_LOCAL_ORIGIN);
	};

	class Triangle : public BaseObject
	{
		public:
			Triangle();
			~Triangle();
	};

	class Plane : public BaseObject
	{
		public:
			Plane();
			~Plane();
	};
};
