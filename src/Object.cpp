#include "Object.h"

using namespace Object;

BaseObject::BaseObject() : m_mat4_model(glm::mat4(1.0f)), m_mat4_translate(glm::mat4(1.0f)),
	m_mat4_rotate(glm::mat4(1.0f)), m_mat4_scale(glm::mat4(1.0f))
{

}

BaseObject::~BaseObject()
{
	glDeleteBuffers(1, &mVertexbuffer);
}

template <int N, int M>
void BaseObject::useShaderChar(GLchar const *(&v_source)[N], GLchar const *(&f_source)[M])
{
	mProgram->runChar(v_source, f_source);
}

void BaseObject::useShaderFile(std::string vertexFile, std::string fragmentFile)
{
	mProgram->runFile(vertexFile, fragmentFile);
}

glm::mat4 BaseObject::getModelMatrix()
{
	m_mat4_model = m_mat4_translate * m_mat4_rotate * m_mat4_scale;
	return m_mat4_model;
}

void BaseObject::render(glm::mat4 mat_PV)
{
	// MVP matrix
	mProgram->use();

	m_mat4_model = m_mat4_translate * m_mat4_rotate * m_mat4_scale;
	glm::mat4 mat_MVP = mat_PV * m_mat4_model;
	GLuint matrixID = glGetUniformLocation(mProgram->getID(), "MVP");
	glUniformMatrix4fv(matrixID, 1, GL_FALSE, &mat_MVP[0][0]);

	GLuint scaleID = glGetUniformLocation(mProgram->getID(), "objScale");
	glUniform3f(scaleID, m_mat4_scale[0][0], m_mat4_scale[1][1], m_mat4_scale[2][2]);

	// Get the struct going
	glEnableVertexAttribArray(0); // xyz
	glEnableVertexAttribArray(1); // nx ny nz
	glEnableVertexAttribArray(2); // uv
	glEnableVertexAttribArray(3); // barycentric x,y,z

	glBindBuffer(GL_ARRAY_BUFFER, mVertexbuffer);

	// Send the struct over
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0); // x,y,z
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0); // nx, ny, nz
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0); // u,v

	glBindBuffer(GL_ARRAY_BUFFER, mBarymetricBuffer);
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (void*)0); // barycentric x,y,z

	glDrawArrays(GL_TRIANGLES, 0, mVertices.size());

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(3);
}

BaseObject* BaseObject::translateBy(glm::vec3 pos, Mode mode)
{
	m_mat4_translate = glm::translate(m_mat4_translate, glm::vec3(pos.x,pos.y,pos.z));
	return this;
}

BaseObject* BaseObject::translateTo(glm::vec3 pos, Mode mode)
{
	m_mat4_translate = glm::mat4(1.0f);
	return translateBy(pos);
}

BaseObject* BaseObject::rotateBy(float angle, glm::vec3 axis, Mode mode)
{
	m_mat4_rotate = glm::rotate(m_mat4_rotate, angle, axis);
	return this;
}

BaseObject* BaseObject::rotateTo(float angle, glm::vec3 axis, Mode mode)
{
	m_mat4_rotate = glm::mat4(1.0f);
	return rotateBy(angle, axis);
}

BaseObject* BaseObject::scaleBy(glm::vec3 scale, Mode mode)
{
	m_mat4_scale = glm::scale(m_mat4_scale, scale);
	return this;
}

BaseObject* BaseObject::scaleTo(glm::vec3 scale, Mode mode)
{
	m_mat4_scale = glm::mat4(1.0f);
	return scaleBy(scale);
}

Triangle::Triangle() : BaseObject()
{
	mObjectType = "Triangle";

	mProgram = std::make_unique<Shader_prog>();
	mProgram->runFile("vertexshader", "fragmentshader_green");

	Log() << "creating triangle";
	// bottom left - extrapolate the rest
	Vertex bl = Vertex(-1.0f, -1.0f, 0.0f);
	Vertex br = Vertex(1.0f, -1.0f, 0.0f);
	Vertex t = Vertex(0.0f, 1.0f, 0.0f);

	mVertices.push_back(bl);
	mVertices.push_back(br);
	mVertices.push_back(t);

	glGenBuffers(1, &mVertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, mVertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, mVertices.size() * sizeof(Vertex), &mVertices.front(), GL_STATIC_DRAW);
}

Triangle::~Triangle()
{
	glDeleteBuffers(1, &mVertexbuffer);
}

Plane::Plane() : BaseObject()
{
	mObjectType = "Plane";

	mProgram = std::make_unique<Shader_prog>();
	mProgram->runFile("shaders/simple.vsh", "shaders/grid.fsh");

	Log() << "creating plane";
	// bottom left - extrapolate the rest
	Vertex bl = Vertex(-1.0f, -1.0f, 0.0f);
	Vertex br = Vertex(1.0f, -1.0f, 0.0f);
	Vertex tr = Vertex(1.0f, 1.0f, 0.0f);
	Vertex tl = Vertex(-1.0f, 1.0f, 0.0f);

	mVertices.push_back(bl);
	mVertices.push_back(br);
	mVertices.push_back(tr);
	mVertices.push_back(tr);
	mVertices.push_back(tl);
	mVertices.push_back(bl);

	std::vector<float> barymetric;
	for(unsigned long i=0; i<mVertices.size()/3; ++i) // two triangles in this case
	{
		barymetric.push_back(1.0f);
		barymetric.push_back(0.0f);
		barymetric.push_back(0.0f);
		barymetric.push_back(0.0f);
		barymetric.push_back(1.0f);
		barymetric.push_back(0.0f);
		barymetric.push_back(0.0f);
		barymetric.push_back(0.0f);
		barymetric.push_back(1.0f);
	}

	glGenBuffers(1, &mVertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, mVertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, mVertices.size() * sizeof(Vertex), &mVertices.front(), GL_STATIC_DRAW);

	glGenBuffers(1, &mBarymetricBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, mBarymetricBuffer);
	glBufferData(GL_ARRAY_BUFFER, barymetric.size() * sizeof(float), &barymetric.front(), GL_STATIC_DRAW);
}

Plane::~Plane()
{
	glDeleteBuffers(1, &mVertexbuffer);
}
