#version 330 core
layout(location = 0) in vec2 pos;
layout(location = 1) in vec2 uv;
layout(location = 2) in vec2 norm;

out vec2 uv_frag;
out vec2 norm_frag;

void main()
{
	gl_Position = vec4(pos.x, pos.y, 0, 1);
	uv_frag = uv;
	norm_frag = norm;
}
