#pragma once

#include <algorithm>
#include <vector>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/vector_angle.hpp>

// my own stuff
#include "gltext/gltext.hpp"
#include "defines.h"
#include "Image.h"
#include "Input.h"
#include "Logger.h"
#include "Shader.h"
#include "Vertex.h"

namespace GUI
{
	class Element
	{
		friend class Node;
		friend class Window;
		private:
		protected:
			virtual void input() {}
			virtual void update() {}
			virtual void render() {}

			ptr_u<Shader_prog> mProgram;

		public:
			Element();
			~Element();

			void setShader(std::string vFile, std::string fFile);
	};

	class Node
	{
		private:
			GLFWwindow* mWindow;
			std::vector<ptr_s<Element>> mMembers;

		public:
			Node();
			~Node();

			void add(ptr_s<Element> element);

			void render();
	};

	enum ImageType {
		IMAGE_FLOAT = 1 << 0, // image will not be stretched in any way
		IMAGE_FILL = 1 << 1, // image will fill the entire box completely
		IMAGE_PROPFILL = 1 << 2, // image will fill the box as much as possible, but keep proportions
	};

	class BackgroundImage : public Element
	{
		private:
			GLuint mVertexbuffer;
			GLuint mTexture = 0;
			GLuint mTextureSampler;

			ptr_u<Image> mImage;
			ImageType mImageType;

			int mPosx;
			int mPosy;
			int mWidth;
			int mHeight;

			std::vector<Vertex2D> mVertices;

			void setBuffer();
			void setTexture();

		protected:
			void render();

		public:
			BackgroundImage(std::string img, int x=0, int y=0, int w=0, int h=0);
			~BackgroundImage();

			void setImage(std::string file);
			void setImageType(ImageType imgtp);
	};

	class Window : public Element
	{
		private:
			ptr_u<Shader_prog> mProgram;
			GLuint mVertexbuffer;
			BackgroundImage* mBgImg;
			std::vector<Element*> mChildren;

		protected:
			void render();

		public:
			Window();
			~Window();
	};

}
