#include "Image.h"
// image loader
#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>

/*
===========================================
Image Object

load an Image from a file
===========================================
*/
Image::Image(std::string file) : mFile(file)
{

}

Image::~Image()
{
	delete mData;
}

/*
===========================================
loadFile

load Image data from file and save it into mData
===========================================
*/
unsigned char* Image::loadFile(std::string file)
{
	if(file != "")
	{
		mFile = file;
	}

	mData = stbi_load(mFile.c_str(), &mWidth, &mHeight, &mBpp, 0);
	return mData;
}

/*
===========================================
getFile
===========================================
*/
std::string Image::getFile()
{
	return mFile;
}

/*
===========================================
getData
===========================================
*/
unsigned char* Image::getData()
{
	return mData;
}

/*
===========================================
getWidth
===========================================
*/
int Image::getWidth()
{
	return mWidth;
}

/*
===========================================
getHeight
===========================================
*/
int Image::getHeight()
{
	return mHeight;
}

/*
===========================================
getBpp
===========================================
*/
int Image::getBpp()
{
	return mBpp;
}

/*
===========================================
setFile
===========================================
*/
void Image::setFile(std::string file)
{
	mFile = file;
}
