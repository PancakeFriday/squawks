#include "GUI.h"

using namespace GUI;

/*
===========================================
Element Object

The base class for GUI elements
===========================================
*/
Element::Element()
{

}

Element::~Element()
{

}


/*
===========================================
setShader

Set a new vertex and fragment shader
===========================================
*/
void Element::setShader(std::string vFile, std::string fFile)
{
	mProgram->runFile(vFile, fFile);
}

/*
===========================================
Node Object

Every GUI element needs a main node for rendering and updating
===========================================
*/
Node::Node()
{

}

Node::~Node()
{

}

/*
===========================================
add

Add an element to the node object
===========================================
*/
void Node::add(ptr_s<Element> element)
{
	mMembers.push_back(element);
}

/*
===========================================
render

Cycle through all children and render
===========================================
*/
void Node::render()
{
	for(auto it=mMembers.begin(); it != mMembers.end(); ++it)
	{
		(*it)->render();
	}
}

/*
===========================================
BackgroundImage Object

This object is a box with a background.
===========================================
*/
BackgroundImage::BackgroundImage(std::string img, int x, int y, int w, int h) : mPosx(x), mPosy(y), mWidth(w), mHeight(h)
{
	mProgram = std::make_unique<Shader_prog>();

	setImage(img);
	setImageType(IMAGE_FLOAT);
	setShader("shaders/simple2D.vsh", "shaders/tex.fsh");
}

BackgroundImage::~BackgroundImage()
{

}

/*
===========================================
setBuffer

create a buffer and bind it
===========================================
*/
void BackgroundImage::setBuffer()
{
	float umax = 1; // this is IMAGE_FILL
	float vmax = 1;
	if(mImageType & IMAGE_FLOAT)
	{
		umax = mWidth / (float)mImage->getWidth();
		vmax = mHeight / (float)mImage->getHeight();
	}
	else if(mImageType & IMAGE_PROPFILL)
	{
		float imProp = mImage->getWidth() / mImage->getHeight();
		float winProp = (float)mWidth / (float)mHeight;
		if(mWidth * imProp < mHeight)
		{
			Log() << "ya";
			// stretch to width
			umax = 1;
			vmax /= winProp;
			Log() << vmax;
		}
		else
		{
			vmax = 1;
			umax *= winProp;
		}
	}

	// funnily enough, the images are upside down. So UV has the coordinate origin in the top left
	glGenBuffers(1, &mVertexbuffer);
	Vertex2D tl(mPosx, mPosy,0,0);
	Vertex2D bl(mPosx, mPosy + mHeight,0,vmax);
	Vertex2D br(mPosx + mWidth, mPosy + mHeight,umax,vmax);
	Vertex2D tr(mPosx + mWidth, mPosy,umax,0);

	mVertices.clear();

	mVertices.push_back(tl);
	mVertices.push_back(bl);
	mVertices.push_back(br);
	mVertices.push_back(br);
	mVertices.push_back(tr);
	mVertices.push_back(tl);

	glBindBuffer(GL_ARRAY_BUFFER, mVertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, mVertices.size() * sizeof(Vertex2D), &mVertices.front(), GL_STATIC_DRAW);
}

/*
===========================================
setTexture

set a texture for the box
===========================================
*/
void BackgroundImage::setTexture()
{
	unsigned char* data = mImage->loadFile();

	// Create one OpenGL texture
	glGenTextures(1, &mTexture);

	mTextureSampler  = glGetUniformLocation(mProgram->getID(), "TextureSampler");

	// "Bind" the newly created texture : all future texture functions will modify this texture
	glBindTexture(GL_TEXTURE_2D, mTexture);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER );

	// Give the image to OpenGL
	glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA, mImage->getWidth(), mImage->getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
}

/*
===========================================
render
===========================================
*/
void BackgroundImage::render()
{
	if(mTexture == 0)
	{
		Log(Error) << "BackgroundImage has no texture. Did you load an image? Aborting.";
		exit(EXIT_FAILURE);
	}

	mProgram->use();

	// Bind our texture in Texture Unit 0
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, mTexture);
	glUniform1i(mTextureSampler, 0);

	// Get the struct going
	glEnableVertexAttribArray(0); // xy
	glEnableVertexAttribArray(1); // uv
	glEnableVertexAttribArray(2); // nx ny

	glBindBuffer(GL_ARRAY_BUFFER, mVertexbuffer);

	// Bind our texture in Texture Unit 0
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, mTexture);
	// Set our "myTextureSampler" sampler to user Texture Unit 0
	glUniform1i(mTextureSampler, 0);

	// Send the struct over
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex2D), reinterpret_cast<const GLvoid*>( offsetof(Vertex2D, x))); // x,y
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex2D), reinterpret_cast<const GLvoid*>( offsetof(Vertex2D, u))); // u,v
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex2D), reinterpret_cast<const GLvoid*>( offsetof(Vertex2D, nx))); // nx,ny

	glDrawArrays(GL_TRIANGLES, 0, mVertices.size());

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
}

/*
===========================================
setImage

create the background Image for the box
===========================================
*/
void BackgroundImage::setImage(std::string file)
{
	if(!mImage)
	{
		mImage = std::make_unique<Image>(file);
	}
	else
	{
		mImage->setFile(file);
	}
	setTexture();
}

/*
===========================================
setImageType

set whether the image is being filled in the box or not
Only call this once! setBuffer is heavy.
===========================================
*/
void BackgroundImage::setImageType(ImageType imgtp)
{
	mImageType = imgtp;
	setBuffer();
}

/*
===========================================
Window Object

A window contains other GUI elements
===========================================
*/
Window::Window()
{
}

Window::~Window()
{

}

/*
===========================================
render
===========================================
*/
void Window::render()
{

}
